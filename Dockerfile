FROM yannoff/maven:3-openjdk-19 as build
COPY . /app
COPY ./pom.xml /app

WORKDIR /app
RUN mvn -f pom.xml clean install -DskipTests

FROM yannoff/maven:3-openjdk-19
COPY --from=build /app/target/catsFoodShop-1.0-SNAPSHOT.jar cats-food-shop.jar
ENTRYPOINT [ "java", "-jar", "cats-food-shop.jar"]