package com.lesson.shop;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;

public class TestResourceReader {

    public static String readJsonFromResources(String pathToFileInClassPath) throws IOException {
        Resource resource = new ClassPathResource(pathToFileInClassPath);
        JsonNode jsonNode = new ObjectMapper().readTree(resource.getURL());
        return jsonNode.toString();
    }
}
