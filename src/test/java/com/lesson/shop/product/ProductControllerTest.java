package com.lesson.shop.product;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lesson.shop.TestResourceReader;
import com.lesson.shop.config.TestPersistenceConfig;
import com.lesson.shop.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ContextConfiguration(classes = TestPersistenceConfig.class)
@AutoConfigureMockMvc(addFilters = false)
@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
        "/data/sql/user-seed-data.sql",
        "/data/sql/order/order-seed-data.sql"
})
@WithMockUser(username = "admin", authorities = {"ADMIN"})
public class ProductControllerTest {
    private final static String USER_PRODUCT_BASE_URL = "/user/products";
    private final static String ADMIN_PRODUCT_BASE_URL = "/admin/products";

    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private ProductRepository productRepository;

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void getAllProductsForUserSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(USER_PRODUCT_BASE_URL)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getAllProductsForUserSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void getProductsForUserByNameSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(USER_PRODUCT_BASE_URL)
                        .param("name", "product1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getProductForUserByNameSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void getProductsForUserByPriceSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(USER_PRODUCT_BASE_URL)
                        .param("price", "1236")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getProductForUserByPriceSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void getProductsForUserByNameAndPriceSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(USER_PRODUCT_BASE_URL)
                        .param("name", "product5")
                        .param("price", "1236")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getProductForUserByNameAndPriceSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void getAllProductsForAdminSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ADMIN_PRODUCT_BASE_URL)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getAllProductsForAdminSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void getProductForAdminByNameSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ADMIN_PRODUCT_BASE_URL)
                        .param("name", "product1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getProductForAdminByNameSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void getProductForAdminByPriceSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ADMIN_PRODUCT_BASE_URL)
                        .param("price", "1236")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getProductForAdminByPriceSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void getProductForAdminByNameAndPriceSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ADMIN_PRODUCT_BASE_URL)
                        .param("name", "product5")
                        .param("price", "1236")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/getProductForAdminByNameAndPriceSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void deleteProductSuccess204Test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(ADMIN_PRODUCT_BASE_URL + "/400")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        assertFalse(productRepository.existsById(400L));
    }

    @Test
    public void deleteProductNotExist404Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.delete(ADMIN_PRODUCT_BASE_URL + "/333")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/deleteProductNotExist404Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void deleteProductExistsInOrder404Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.delete(ADMIN_PRODUCT_BASE_URL + "/100")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/deleteProductExistsInOrder400Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void createProductSuccess201Test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ADMIN_PRODUCT_BASE_URL)
                        .content(TestResourceReader.readJsonFromResources("/data/json/product/request/createProductSuccess201Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/createProductSuccess201Response.json"),
                objectMapper.writeValueAsString(productRepository.findByName("createTest")),
                true
        );
    }

    @Test
    public void createProductNameAlreadyExist400Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.post(ADMIN_PRODUCT_BASE_URL)
                        .content(TestResourceReader.readJsonFromResources("/data/json/product/request/createProductNameAlreadyExist400Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/createProductNameAlreadyExist400Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void updateProductSuccess204Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.patch(ADMIN_PRODUCT_BASE_URL + "/400")
                        .content(TestResourceReader.readJsonFromResources("/data/json/product/request/updateProductSuccess204Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/updateProductSuccess204Response.json"),
                objectMapper.writeValueAsString(productRepository.findByName("productTest")),
                false
        );
    }

    @Test
    public void updateProductNotExist404Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.patch(ADMIN_PRODUCT_BASE_URL + "/65")
                        .content(TestResourceReader.readJsonFromResources("/data/json/product/request/updateProductNotExist404Response.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/updateProductNotExist404Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void updateProductNameAlreadyExist400Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.patch(ADMIN_PRODUCT_BASE_URL + "/200")
                        .content(TestResourceReader.readJsonFromResources("/data/json/product/request/updateProductNameAlreadyExist400TRequest.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/product/response/updateProductNameAlreadyExist400Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }
}
