package com.lesson.shop.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testcontainers.containers.JdbcDatabaseContainer;

import javax.sql.DataSource;

@TestConfiguration
@EnableTransactionManagement
@PropertySource("classpath:application.yml")
public class TestPersistenceConfig {

    @Value("${test-container.postgresImageVersion}")
    private String postgresImageVersion;

    @Bean
    @Primary
    public DataSource dataSource(@Qualifier("gateway-test-hikari-config") HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean(initMethod = "start")
    public JdbcDatabaseContainer<?> postgresContainer() {
        return TestPostgresContainer.getInstance(postgresImageVersion);
    }

    @Bean
    @Primary
    @Qualifier("gateway-test-hikari-config")
    public HikariConfig hikariConfig(JdbcDatabaseContainer<?> postgresContainer) {
        HikariConfig config = new HikariConfig();
        config.setUsername(postgresContainer.getUsername());
        config.setPassword(postgresContainer.getPassword());
        config.setJdbcUrl(postgresContainer.getJdbcUrl());
        config.setDriverClassName("org.postgresql.Driver");

        return config;
    }
}