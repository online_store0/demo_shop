package com.lesson.shop.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lesson.shop.TestResourceReader;
import com.lesson.shop.config.TestPersistenceConfig;
import com.lesson.shop.model.entity.OrderEntity;
import com.lesson.shop.model.entity.OrderProductEntity;
import com.lesson.shop.model.entity.ProductEntity;
import com.lesson.shop.repository.OrderProductRepository;
import com.lesson.shop.repository.OrderRepository;
import com.lesson.shop.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootTest
@ContextConfiguration(classes = TestPersistenceConfig.class)
@AutoConfigureMockMvc(addFilters = false)
@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
        "/data/sql/user-seed-data.sql",
        "/data/sql/order/order-seed-data.sql"
})
@WithMockUser(username = "user", authorities = {"USER"})
class UserOrderControllerTest {

    private final static String ORDER_BASE_URL = "/user/orders";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderProductRepository orderProductRepository;

    @Autowired
    private ProductRepository productRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void getOrderSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ORDER_BASE_URL)
                        .accept(MediaType.APPLICATION_JSON)
                        .requestAttr("userId", 4)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/getAllUserOrders200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void createOrderEmptyProduct400Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.post(ORDER_BASE_URL)
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/createOrderEmptyProducts400Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .requestAttr("userId", 4)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/createOrderEmptyProducts400Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void createOrderProductNotExist404Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.post(ORDER_BASE_URL)
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/createOrderProductNotExist404Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .requestAttr("userId", 4)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/createOrderProductNotExist404Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void createOrderProductHaveNotEnoughCapacity400Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.post(ORDER_BASE_URL)
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/createOrderProductHaveNotEnoughCapacity400Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .requestAttr("userId", 4)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/createOrderProductHaveNotEnoughCapacity400Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void createOrderSuccess201Test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ORDER_BASE_URL)
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/createOrderSuccess201Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .requestAttr("userId", 4)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        OrderEntity createdOrder = orderRepository.getReferenceById(1L);
        List<OrderProductEntity> createdOrderProductEntities = orderProductRepository.findAllByOrderId(createdOrder.getId());
        List<ProductEntity> createdOrderProducts = productRepository.findAllById(
                createdOrderProductEntities.stream()
                        .map(OrderProductEntity::getProductId)
                        .collect(Collectors.toList())
        );

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/createOrderSuccessOrder201Response.json"),
                objectMapper.writeValueAsString(createdOrder),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("date", (o1, o2) -> true)
                )
        );

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/createOrderSuccessOrderProducts201Response.json"),
                objectMapper.writeValueAsString(createdOrderProductEntities),
                false
        );

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/createOrderSuccessProducts201Response.json"),
                objectMapper.writeValueAsString(createdOrderProducts),
                false
        );
    }

}
