package com.lesson.shop.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lesson.shop.TestResourceReader;
import com.lesson.shop.config.TestPersistenceConfig;
import com.lesson.shop.repository.OrderProductRepository;
import com.lesson.shop.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ContextConfiguration(classes = TestPersistenceConfig.class)
@AutoConfigureMockMvc(addFilters = false)
@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
        "/data/sql/user-seed-data.sql",
        "/data/sql/order/order-seed-data.sql"
})
@WithMockUser(username = "admin", authorities = {"ADMIN"})
public class AdminOrderControllerTest {

    private final static String ORDER_BASE_URL = "/admin/orders";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private OrderRepository orderRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private OrderProductRepository orderProductRepository;

    @Test
    public void getAllOrdersSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ORDER_BASE_URL)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/getAllOrdersSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void getAllUserOrdersSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ORDER_BASE_URL)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("userId", "4")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/getAllUserOrders200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void getOrderProductsSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ORDER_BASE_URL + "/100")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/getOrderProductsSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                true
        );
    }

    @Test
    public void deleteOrderSuccessTest() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.delete(ORDER_BASE_URL + "/100")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        assertFalse(orderRepository.existsById(100L));
    }

    @Test
    public void deleteOrderNotExist404Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.delete(ORDER_BASE_URL + "/333")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/deleteOrderNotExist404Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void updateOrderSuccessTest() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.patch(ORDER_BASE_URL + "/100")
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/updateOrderRequest.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        JSONAssert.assertEquals(TestResourceReader.readJsonFromResources("/data/json/order/response/updateOrderSuccessProducts204Response.json"),
                objectMapper.writeValueAsString(orderRepository.getAllOrderProductById(100L)),
                false
        );

        JSONAssert.assertEquals(TestResourceReader.readJsonFromResources("/data/json/order/response/updateOrderSuccessOrderProducts204Response.json"),
                objectMapper.writeValueAsString(orderProductRepository.findAllByOrderId(100L)),
                false
        );
    }

    @Test
    public void updateOrderDoesNotExist404test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.patch(ORDER_BASE_URL + "/101")
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/updateOrderRequest.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/updateOrderNotExist404Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void updateOrderProductNotExist404Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.patch(ORDER_BASE_URL + "/100")
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/updateOrderProductNotExist404Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/updateOrderProductNotExist404Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }

    @Test
    public void updateOrderProductHaveNotEnoughCapacity400Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.patch(ORDER_BASE_URL + "/100")
                        .content(TestResourceReader.readJsonFromResources("/data/json/order/request/updateOrderProductHaveNotEnoughCapacity400Request.json"))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/order/response/updateOrderProductHaveNotEnoughCapacity400Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }
}
