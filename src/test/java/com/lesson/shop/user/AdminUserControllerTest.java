package com.lesson.shop.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lesson.shop.TestResourceReader;
import com.lesson.shop.config.TestPersistenceConfig;
import com.lesson.shop.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ContextConfiguration(classes = TestPersistenceConfig.class)
@AutoConfigureMockMvc(addFilters = false)
@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
        "/data/sql/user-seed-data.sql",
        "/data/sql/order/order-seed-data.sql"
})
@WithMockUser(username = "admin", authorities = {"ADMIN"})
public class AdminUserControllerTest {
    private final static String ADMIN_USER_BASE_URL = "/admin/users";

    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private UserRepository userRepository;

    @Test
    public void getAllUsersSuccess200Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.get(ADMIN_USER_BASE_URL)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/user/response/getAllUsersSuccess200Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void deleteUserSuccess204Test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(ADMIN_USER_BASE_URL + "/2")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        assertFalse(userRepository.existsById(2L));
    }

    @Test
    public void deleteUserNotExist404Test() throws Exception {
        ResultActions actions = mockMvc.perform(MockMvcRequestBuilders.delete(ADMIN_USER_BASE_URL + "/69")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        JSONAssert.assertEquals(
                TestResourceReader.readJsonFromResources("/data/json/user/response/deleteUserNotExist404Response.json"),
                actions.andReturn().getResponse().getContentAsString(),
                new CustomComparator(
                        JSONCompareMode.STRICT,
                        new Customization("meta.timestamp", (o1, o2) -> true)
                )
        );
    }
}
