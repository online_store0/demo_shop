-- USERS
insert into users (id, name, phone_number, password, role_id)
values (4, 'TEST_USER1', '+380123123123', 'dqowdjkqw-odjkqw-0kdqw-dk0', 1);

insert into users (id, name, phone_number, password, role_id)
values (2, 'TEST_USER2', '+3823423123123', 'sd-sda-0kdqw-dk0', 1);

insert into users (id, name, phone_number, password, role_id)
values (3, 'TEST_ADMIN', '+2323232323', 'ssdsdsdsdfsdf', 2);