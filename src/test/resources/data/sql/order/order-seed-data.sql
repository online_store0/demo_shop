-- PRODUCTS
insert into product (id, name, price, "count")
values (100, 'product1', 123, 2);

insert into product (id, name, price, "count")
values (200, 'product2', 1234, 3);

insert into product (id, name, price, "count")
values (300, 'product3', 1235, 4);

insert into product (id, name, price, "count")
values (400, 'product4', 1236, 5);

insert into product (id, name, price, "count")
values (500, 'product5', 1236, 6);

insert into product (id, name, price, "count")
values (600, 'product6', 1238, 7);


-- ORDERS
insert into orders (id, user_id, "date", total_price, order_status)
values (100, 4, '2022-12-12 00:00:00', 5, 'NEW');

insert into orders (id, user_id, "date", total_price, order_status)
values (200, 4, '2022-12-12 01:00:00', 6, 'PROCESSING');

insert into orders (id, user_id, "date", total_price, order_status)
values (300, 4, '2022-12-12 02:00:00', 7, 'FULFILLED');

insert into orders (id, user_id, "date", total_price, order_status)
values (400, 2, '2022-12-12 03:00:00', 8, 'NEW');

insert into orders (id, user_id, "date", total_price, order_status)
values (500, 2, '2022-12-12 04:00:00', 9, 'PROCESSING');

-- ORDER PRODUCTS
insert into order_product (id, order_id, product_id, "count")
values (100, 100, 100, 2);

insert into order_product (id, order_id, product_id, "count")
values (200, 100, 200, 2);

insert into order_product (id, order_id, product_id, "count")
values (300, 200, 500, 8);

insert into order_product (id, order_id, product_id, "count")
values (400, 300, 600, 1);

insert into order_product (id, order_id, product_id, "count")
values (500, 400, 100, 1);

insert into order_product (id, order_id, product_id, "count")
values (600, 400, 200, 3);

insert into order_product (id, order_id, product_id, "count")
values (700, 500, 300, 300);


