package com.lesson.shop.controller;

import com.lesson.shop.model.OrderModel;
import com.lesson.shop.model.OrderProductModel;
import com.lesson.shop.model.request.OrderUpdateRequest;
import com.lesson.shop.service.order.impl.AdminOrderService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.lesson.shop.constants.Constants.SECURITY_SCHEME_NAME;

@RestController
@AllArgsConstructor
@SecurityRequirement(name = SECURITY_SCHEME_NAME)
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminOrderController {

    private final AdminOrderService adminOrderService;

    @GetMapping("/admin/orders")
    public List<OrderModel> getAll(@RequestParam(required = false) Long userId) {
        return adminOrderService.getAllOrders(userId);
    }

    @GetMapping("/admin/orders/{id}")
    public List<OrderProductModel> getOrderProduct(@PathVariable Long id) {
        return adminOrderService.getAllOrderProductById(id);
    }

    @DeleteMapping("/admin/orders/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        adminOrderService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("/admin/orders/{id}")
    public ResponseEntity<Void> updateOrder(@PathVariable Long id, @RequestBody OrderUpdateRequest orderUpdateRequest) {
        adminOrderService.updateOrder(id, orderUpdateRequest);
        return ResponseEntity.noContent().build();
    }
}
