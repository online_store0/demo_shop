package com.lesson.shop.controller;

import com.lesson.shop.model.request.AuthenticationRequest;
import com.lesson.shop.model.request.UserRequest;
import com.lesson.shop.model.response.JwtAuthenticationResponse;
import com.lesson.shop.service.AuthService;
import com.lesson.shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final UserService userService;
    private final AuthService authService;

    @PostMapping("/authentication/register")
    public ResponseEntity<Void> register(@RequestBody UserRequest request) {
        userService.register(request);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/authentication/login")
    public ResponseEntity<JwtAuthenticationResponse> login(@RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authService.login(request));
    }

}
