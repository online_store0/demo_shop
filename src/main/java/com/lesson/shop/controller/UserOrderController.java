package com.lesson.shop.controller;

import com.lesson.shop.model.OrderModel;
import com.lesson.shop.model.request.OrderRequest;
import com.lesson.shop.service.order.impl.UserOrderService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.lesson.shop.constants.Constants.SECURITY_SCHEME_NAME;
import static com.lesson.shop.constants.Constants.USER_ID_ATTRIBUTE;

@RestController
@AllArgsConstructor
@Validated
@PreAuthorize("hasAuthority('USER')")
@SecurityRequirement(name = SECURITY_SCHEME_NAME)
public class UserOrderController {

    private final UserOrderService userOrderService;

    @PostMapping("/user/orders")
    public ResponseEntity<Void> create(
            @RequestBody OrderRequest request,
            @RequestAttribute(USER_ID_ATTRIBUTE) Long userId
    ) {
        userOrderService.create(request, userId);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/user/orders")
    public List<OrderModel> getAll(@RequestAttribute(USER_ID_ATTRIBUTE) Long userId) {
        return userOrderService.getAllUserOrders(userId);
    }

}
