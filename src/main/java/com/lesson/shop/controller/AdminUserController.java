package com.lesson.shop.controller;

import com.lesson.shop.model.response.UserResponse;
import com.lesson.shop.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static com.lesson.shop.enums.Constants.SECURITY_SCHEME_NAME;

@RestController
@AllArgsConstructor
@Validated
@SecurityRequirement(name = SECURITY_SCHEME_NAME)
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminUserController {

    private final UserService userService;

    @GetMapping("/admin/users")
    public List<UserResponse> getAll(
            @Valid @RequestParam(required = false) String name,
            @Valid @RequestParam(required = false) String phoneNumber
    ) {
        return userService.getAll(name, phoneNumber);
    }

    @DeleteMapping("/admin/users/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        userService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
