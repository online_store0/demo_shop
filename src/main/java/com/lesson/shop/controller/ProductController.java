package com.lesson.shop.controller;

import com.lesson.shop.model.ProductModel;
import com.lesson.shop.model.entity.ProductEntity;
import com.lesson.shop.model.request.ProductRequest;
import com.lesson.shop.service.ProductService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;

import static com.lesson.shop.constants.Constants.SECURITY_SCHEME_NAME;

@RestController
@AllArgsConstructor
@Validated
@SecurityRequirement(name = SECURITY_SCHEME_NAME)
public class ProductController {

    private final ProductService productService;

    @GetMapping("/admin/products")
    @PreAuthorize("hasAuthority('ADMIN')")
    @Parameter(in = ParameterIn.QUERY, name = "page", schema = @Schema(type = "integer", defaultValue = "0"))
    @Parameter(in = ParameterIn.QUERY, name = "size", schema = @Schema(type = "integer", defaultValue = "15"))
    public Page<ProductEntity> getAll(
            @Valid @RequestParam(required = false) String name,
            @Valid @RequestParam(required = false) BigDecimal price,
            @Parameter(hidden = true) Pageable pageable
    ) {
        return productService.getAll(pageable, name, price);
    }

    @GetMapping("/user/products")
    @PreAuthorize("hasAuthority('USER')")
    @Parameter(in = ParameterIn.QUERY, name = "page", schema = @Schema(type = "integer", defaultValue = "0"))
    @Parameter(in = ParameterIn.QUERY, name = "size", schema = @Schema(type = "integer", defaultValue = "15"))
    public Page<ProductModel> getAllProducts(
            @Valid @RequestParam(required = false) String name,
            @Valid @RequestParam(required = false) BigDecimal price,
            @Parameter(hidden = true) Pageable pageable
    ) {
        return productService.getAllProductModel(pageable, name, price);
    }

    @DeleteMapping("/admin/products/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        productService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/admin/products")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Void> create(@RequestBody ProductRequest request) {
        productService.create(request);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping("/admin/products/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Void> update(
            @PathVariable Long id,
            @RequestBody @Valid ProductRequest request
    ) {
        productService.update(id, request);
        return ResponseEntity.noContent().build();
    }
}
