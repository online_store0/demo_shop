package com.lesson.shop.repository;

import com.lesson.shop.model.entity.RoleEntity;
import com.lesson.shop.model.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    RoleEntity findFirstByName(Role role);

}
