package com.lesson.shop.repository;

import com.lesson.shop.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    List<UserEntity> findByName(String name);

    Optional<UserEntity> findByPhoneNumber(String phoneNumber);

    List<UserEntity> findByNameAndPhoneNumber(String name, String phoneNumber);

    boolean existsByPhoneNumber(String phoneNumber);

}
