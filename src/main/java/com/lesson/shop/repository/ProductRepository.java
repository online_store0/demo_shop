package com.lesson.shop.repository;

import com.lesson.shop.model.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    @Query(nativeQuery = true, value = "SELECT * from product where (:name IS NULL OR name LIKE %:name%)" +
            " AND (:price IS NULL OR price = :price)")
    Page<ProductEntity> search(String name, BigDecimal price, Pageable pageable);



    boolean existsByName(String name);

    ProductEntity findByName(String createTest);

}
