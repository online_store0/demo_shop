package com.lesson.shop.enums;

public class Constants {
    public static final String ROLE_ATTRIBUTE = "role";
    public static final String USER_ID_ATTRIBUTE = "userId";
    public static final String SECURITY_SCHEME_NAME = "foodshop";
}
