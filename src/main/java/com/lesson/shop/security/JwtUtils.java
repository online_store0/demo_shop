package com.lesson.shop.security;

import com.lesson.shop.model.enums.Role;
import io.jsonwebtoken.Claims;

import java.util.Set;


public class JwtUtils {
    public static JwtAuthentication generate(Claims claims) {
        final JwtAuthentication jwtInfoToken = new JwtAuthentication();
        jwtInfoToken.setRoles(getRoles(claims));
        jwtInfoToken.setUsername(claims.getSubject());
        return jwtInfoToken;
    }

    private static Set<Role> getRoles(Claims claims) {
        final String role = claims.get("role", String.class);
        return Set.of(Role.valueOf(role));
    }

}
