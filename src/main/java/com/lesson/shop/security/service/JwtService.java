package com.lesson.shop.security.service;

import com.lesson.shop.model.entity.UserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Date;

@Service
public class JwtService {

    private final SecretKey jwtSecretKey;

    public JwtService(@Value("${app.jwt.secret}") String jwtSecretKey) {
        this.jwtSecretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecretKey));
    }

    public String generateToken(UserEntity user) {
        return Jwts.builder()
                .id(String.valueOf(user.getId()))
                .claim("role", user.getRole().getName())
                .subject(user.getName())
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000))
                .signWith(jwtSecretKey)
                .compact();
    }

    public boolean validateToken(String token) {
        return validate(token, jwtSecretKey);
    }

    public Claims getAccessClaims(String token) {
        return getClaims(token, jwtSecretKey);
    }

    private Claims getClaims(String token, SecretKey secret) {
        return Jwts.parser()
                .verifyWith(secret)
                .build()
                .parseSignedClaims(token)
                .getPayload();
    }

    private boolean validate(String token, SecretKey secret) {
        try {
            Jwts.parser()
                    .verifyWith(secret)
                    .build()
                    .parseSignedClaims(token);
            return true;
        } catch (Exception exception) {
            System.out.println("token is not valid");
        }
        return false;
    }

}
