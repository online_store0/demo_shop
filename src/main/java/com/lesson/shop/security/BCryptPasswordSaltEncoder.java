package com.lesson.shop.security;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptPasswordSaltEncoder extends BCryptPasswordEncoder {

    public BCryptPasswordSaltEncoder() {
        super();
    }

    public String encode(CharSequence rawPassword, String salt) {
        return BCrypt.hashpw(rawPassword.toString(), salt);
    }

    public String getSalt() {
        return BCrypt.gensalt();
    }
}
