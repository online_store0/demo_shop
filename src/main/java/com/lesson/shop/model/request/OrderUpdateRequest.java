package com.lesson.shop.model.request;

import com.lesson.shop.model.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderUpdateRequest {
    private OrderStatus orderStatus;
    private List<OrderProductRequest> orderProducts;
}
