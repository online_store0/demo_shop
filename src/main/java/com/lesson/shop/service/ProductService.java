package com.lesson.shop.service;

import com.lesson.shop.exception.BadRequestException;
import com.lesson.shop.exception.EntityNotFoundException;
import com.lesson.shop.model.ProductModel;
import com.lesson.shop.model.entity.ProductEntity;
import com.lesson.shop.model.request.ProductRequest;
import com.lesson.shop.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final OrderProductService orderProductService;

    public void create(ProductRequest request) {
        if (productRepository.existsByName(request.getName())) {
            throw new BadRequestException("Product with name " + request.getName() + " already exist");
        }

        ProductEntity product = ProductEntity.builder()
                .name(request.getName())
                .price(request.getPrice())
                .count(request.getCount())
                .build();

        productRepository.save(product);
    }

    public void saveAll(List<ProductEntity> productEntities) {
        productRepository.saveAll(productEntities);
    }

    public Page<ProductEntity> getAll(Pageable pageable, String name, BigDecimal price) {
        return productRepository.search(name, price, pageable);
    }

    public Page<ProductModel> getAllProductModel(Pageable pageable, String name, BigDecimal price) {
        Page<ProductEntity> page = productRepository.search(name, price, pageable);
        List<ProductModel> result = mapToModelList(page.getContent());
        return new PageImpl<>(result,pageable,page.getTotalElements());
    }

    public void deleteById(Long id) {
        if (!productRepository.existsById(id)) {
            throw new EntityNotFoundException("Product with id " + id + " doesn't exist");
        }
        if (orderProductService.existsByProductId(id)) {
            throw new BadRequestException("product with id " + id + " can't be deleted while he exists in the order");
        }
        productRepository.deleteById(id);
    }

    public void update(Long id, ProductRequest request) {
        ProductEntity product = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product with id " + id + " doesn't exist"));

        if (request.getName() != null) {
            if (productRepository.existsByName(request.getName())) {
                throw new BadRequestException("Product with name " + request.getName() + " already exist");
            }
            product.setName(request.getName());
        }

        if (request.getPrice() != null) {
            product.setPrice(request.getPrice());
        }

        if (request.getCount() != null) {
            product.setCount(request.getCount());
        }

        productRepository.save(product);
    }

    public List<ProductEntity> findAllByIds(List<Long> ids) {
        return productRepository.findAllById(ids);
    }

    private List<ProductModel> mapToModelList(List<ProductEntity> entities) {
        return entities.stream()
                .map(productEntity -> ProductModel.builder()
                        .name(productEntity.getName())
                        .price(productEntity.getPrice())
                        .build())
                .collect(Collectors.toList());
    }

}
