package com.lesson.shop.service;

import com.lesson.shop.exception.BadRequestException;
import com.lesson.shop.exception.EntityNotFoundException;
import com.lesson.shop.model.entity.RoleEntity;
import com.lesson.shop.model.entity.UserEntity;
import com.lesson.shop.model.enums.Role;
import com.lesson.shop.model.request.UserRequest;
import com.lesson.shop.model.response.UserResponse;
import com.lesson.shop.repository.RoleRepository;
import com.lesson.shop.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    @Transactional
    public void register(UserRequest userRequest) {
        if (userRepository.existsByPhoneNumber(userRequest.getPhoneNumber())) {
            throw new BadRequestException("user with number " + userRequest.getPhoneNumber() + " already exist");
        }

        RoleEntity role = roleRepository.findFirstByName(Role.USER);

        UserEntity user = UserEntity.builder()
                .name(userRequest.getName())
                .phoneNumber(userRequest.getPhoneNumber())
                .password(passwordEncoder.encode(userRequest.getPassword()))
                .role(role)
                .build();

        userRepository.save(user);
    }

    public List<UserResponse> getAll(String name, String phoneNumber) {
        if (name == null && phoneNumber == null) {
            return mapEntityToResponse(userRepository.findAll());
        }
        if (name != null && phoneNumber == null) {
            return mapEntityToResponse(userRepository.findByName(name));
        }
        if (name == null && phoneNumber != null) {
            List<UserEntity> userEntity = new ArrayList<>();
            userRepository.findByPhoneNumber(phoneNumber).ifPresent(userEntity::add);

            return mapEntityToResponse(userEntity);
        }
        return mapEntityToResponse(userRepository.findByNameAndPhoneNumber(name, phoneNumber));
    }

    public UserEntity getByPhoneNumber(String phoneNumber) {
        return userRepository.findByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new EntityNotFoundException("user with phone number " + phoneNumber + " doesn't exist"));
    }

    public void deleteById(Long id) {
        if (!userRepository.existsById(id)) {
            throw new EntityNotFoundException("user with id " + id + " doesn't exist");
        }
        userRepository.deleteById(id);
    }

    public boolean existById(Long id) {
        return userRepository.existsById(id);
    }

    private List<UserResponse> mapEntityToResponse(List<UserEntity> userEntities) {
        return userEntities.stream()
                .map(userEntity -> UserResponse.builder()
                        .id(userEntity.getId())
                        .name(userEntity.getName())
                        .phoneNumber(userEntity.getPhoneNumber())
                        .build())
                .collect(Collectors.toList());
    }

}
