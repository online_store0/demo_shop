package com.lesson.shop.service;

import com.lesson.shop.model.entity.UserEntity;
import com.lesson.shop.model.request.AuthenticationRequest;
import com.lesson.shop.model.response.JwtAuthenticationResponse;
import com.lesson.shop.security.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final BCryptPasswordEncoder passwordEncoder;
    private final UserService userService;
    private final JwtService jwtService;

    public JwtAuthenticationResponse login(AuthenticationRequest request) {
        UserEntity user = userService.getByPhoneNumber(request.getPhoneNumber());

        if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new RuntimeException("password is not valid");
        }

        return JwtAuthenticationResponse.builder()
                .token(jwtService.generateToken(user))
                .build();
    }

}
