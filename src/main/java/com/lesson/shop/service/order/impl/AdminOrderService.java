package com.lesson.shop.service.order.impl;

import com.lesson.shop.exception.BadRequestException;
import com.lesson.shop.exception.EntityNotFoundException;
import com.lesson.shop.model.OrderModel;
import com.lesson.shop.model.OrderProductModel;
import com.lesson.shop.model.entity.OrderEntity;
import com.lesson.shop.model.entity.OrderProductEntity;
import com.lesson.shop.model.entity.ProductEntity;
import com.lesson.shop.model.request.OrderRequest;
import com.lesson.shop.model.request.OrderUpdateRequest;
import com.lesson.shop.repository.OrderRepository;
import com.lesson.shop.service.OrderProductService;
import com.lesson.shop.service.ProductService;
import com.lesson.shop.service.UserService;
import com.lesson.shop.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AdminOrderService extends OrderService {

    private final OrderRepository orderRepository;
    private final OrderProductService orderProductService;
    private final UserService userService;
    private final ProductService productService;

    @Autowired
    public AdminOrderService(
            OrderRepository orderRepository,
            OrderProductService orderProductService,
            UserService userService,
            ProductService productService
    ) {
        super(orderProductService, productService);
        this.orderRepository = orderRepository;
        this.orderProductService = orderProductService;
        this.userService = userService;
        this.productService = productService;
    }

    public List<OrderModel> getAllOrders(Long userId) {
        if (userId == null) {
            return orderRepository.findAllOrders();
        }

        if (!userService.existById(userId)) {
            throw new EntityNotFoundException("user with id " + userId + " doesn't exist");
        }

        return orderRepository.findAllUserOrders(userId);
    }

    public List<OrderProductModel> getAllOrderProductById(Long id) {
        return orderRepository.getAllOrderProductById(id);
    }

    @Transactional
    public void updateOrder(Long id, OrderUpdateRequest orderUpdateRequest) {
        OrderEntity orderEntity = orderRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Order with id " + id + " doesn't exist"));

        if (orderUpdateRequest.getOrderStatus() != null) {
            orderEntity.setOrderStatus(orderUpdateRequest.getOrderStatus());
            orderRepository.save(orderEntity);
        }

        if (orderUpdateRequest.getOrderProducts() != null) {
            validateOrderUpdateRequest(orderUpdateRequest, getProductEntities(orderUpdateRequest.getOrderProducts()));

            List<OrderProductEntity> orderProductEntities = orderProductService.findAllByOrderId(id);

            List<ProductEntity> initialProductList = productService.findAllByIds(
                    orderProductEntities.stream()
                            .map(OrderProductEntity::getProductId)
                            .collect(Collectors.toList())
            );

            updateOrderProductCount(orderProductEntities, initialProductList);

            orderProductService.deleteOrderProduct(id);

            List<ProductEntity> updatedProductList = getProductEntities(orderUpdateRequest.getOrderProducts());

            orderEntity.setTotalPrice(
                    getTotalPrice(new OrderRequest(orderUpdateRequest.getOrderProducts()), updatedProductList)
            );

            orderRepository.save(orderEntity);

            createOrderProducts(new OrderRequest(orderUpdateRequest.getOrderProducts()), orderEntity);

            updateProductCount(new OrderRequest(orderUpdateRequest.getOrderProducts()), updatedProductList);
        }
    }

    public void deleteById(Long id) {
        if (!orderRepository.existsById(id)) {
            throw new EntityNotFoundException("Order with id " + id + " doesn't exist");
        }
        orderRepository.deleteById(id);
    }

    private void validateOrderUpdateRequest(OrderUpdateRequest orderUpdateRequest, List<ProductEntity> products) {
        Map<Long, ProductEntity> productMap = products.stream()
                .collect(Collectors.toMap(ProductEntity::getId, Function.identity()));

        orderUpdateRequest.getOrderProducts().forEach(product -> {
                    if (!productMap.containsKey(product.getProductId())) {
                        throw new EntityNotFoundException("Product with id = " + product.getProductId() + " doesn't exist");
                    }
                    if (productMap.get(product.getProductId()).getCount() < product.getCount()) {
                        throw new BadRequestException("Product with id = " + product.getProductId() + " doesn't have enough capacity");
                    }
                }
        );
    }
}
