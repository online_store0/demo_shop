package com.lesson.shop.service.order.impl;

import com.lesson.shop.exception.BadRequestException;
import com.lesson.shop.exception.EntityNotFoundException;
import com.lesson.shop.model.OrderModel;
import com.lesson.shop.model.entity.OrderEntity;
import com.lesson.shop.model.entity.ProductEntity;
import com.lesson.shop.model.enums.OrderStatus;
import com.lesson.shop.model.request.OrderRequest;
import com.lesson.shop.repository.OrderRepository;
import com.lesson.shop.service.OrderProductService;
import com.lesson.shop.service.ProductService;
import com.lesson.shop.service.UserService;
import com.lesson.shop.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
@Service
public class UserOrderService extends OrderService {
    private final OrderRepository orderRepository;

    @Autowired
    public UserOrderService(
            OrderRepository orderRepository,
            OrderProductService orderProductService,
            ProductService productService
    ) {
        super(orderProductService, productService);
        this.orderRepository = orderRepository;

    }
    public List<OrderModel> getAllUserOrders(Long userId) {
        return orderRepository.findAllUserOrders(userId);
    }

    @Transactional
    public void create(OrderRequest request, Long userId) {
        List<ProductEntity> products = getProductEntities(request.getProducts());

        validateCreateRequest(request, products);
        OrderEntity orderEntity = createOrder(request, products, userId);
        createOrderProducts(request, orderEntity);
        updateProductCount(request, products);
    }

    private void validateCreateRequest(OrderRequest request, List<ProductEntity> products) {
        if (request.getProducts().isEmpty()) {
            throw new BadRequestException("Products should be specified");
        }

        Map<Long, ProductEntity> productMap = products.stream()
                .collect(Collectors.toMap(ProductEntity::getId, Function.identity()));

        request.getProducts().forEach(requestProduct -> {
            if (!productMap.containsKey(requestProduct.getProductId())) {
                throw new EntityNotFoundException("Product with id = " + requestProduct.getProductId() + " doesn't exist");
            }
            if (productMap.get(requestProduct.getProductId()).getCount() < requestProduct.getCount()) {
                throw new BadRequestException("Product with id = " + requestProduct.getProductId() + " doesn't have enough capacity");
            }
        });
    }

    private OrderEntity createOrder(OrderRequest request, List<ProductEntity> products, Long userId) {
        OrderEntity orderEntity = OrderEntity.builder()
                .userId(userId)
                .date(new Date(Instant.now().toEpochMilli()))
                .totalPrice(getTotalPrice(request, products))
                .orderStatus(OrderStatus.NEW)
                .build();

        return orderRepository.save(orderEntity);
    }
}
