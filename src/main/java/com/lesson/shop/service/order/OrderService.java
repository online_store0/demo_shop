package com.lesson.shop.service.order;

import com.lesson.shop.model.entity.OrderEntity;
import com.lesson.shop.model.entity.OrderProductEntity;
import com.lesson.shop.model.entity.ProductEntity;
import com.lesson.shop.model.request.OrderProductRequest;
import com.lesson.shop.model.request.OrderRequest;
import com.lesson.shop.repository.OrderRepository;
import com.lesson.shop.service.OrderProductService;
import com.lesson.shop.service.ProductService;
import com.lesson.shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderProductService orderProductService;
    private final ProductService productService;

    protected void updateOrderProductCount(List<OrderProductEntity> orderProducts, List<ProductEntity> products) {
        Map<Long, ProductEntity> productMap = products.stream()
                .collect(Collectors.toMap(ProductEntity::getId, Function.identity()));

        for (OrderProductEntity orderProduct : orderProducts) {
            productMap.get(orderProduct.getProductId()).setCount(
                    productMap.get(orderProduct.getProductId()).getCount() + orderProduct.getCount()
            );
        }

        List<ProductEntity> productEntities = new ArrayList<>(productMap.values());
        productService.saveAll(productEntities);
    }

    protected void updateProductCount(OrderRequest request, List<ProductEntity> products) {
        Map<Long, ProductEntity> productMap = products.stream()
                .collect(Collectors.toMap(ProductEntity::getId, Function.identity()));

        for (OrderProductRequest productRequest : request.getProducts()) {
            productMap.get(productRequest.getProductId()).setCount(
                    productMap.get(productRequest.getProductId()).getCount() - productRequest.getCount()
            );
        }
        List<ProductEntity> productEntities = new ArrayList<>(productMap.values());
        productService.saveAll(productEntities);
    }

    protected BigDecimal getTotalPrice(OrderRequest request, List<ProductEntity> products) {
        Map<Long, ProductEntity> productMap = products.stream()
                .collect(Collectors.toMap(ProductEntity::getId, Function.identity()));

        BigDecimal totalPrice = new BigDecimal(0);

        for (OrderProductRequest productRequest : request.getProducts()) {
            totalPrice = totalPrice.add(
                    BigDecimal.valueOf(productRequest.getCount()).multiply(
                            productMap.get(productRequest.getProductId()).getPrice()
                    )
            );
        }
        return totalPrice;
    }

    protected void createOrderProducts(OrderRequest request, OrderEntity orderEntity) {
        List<OrderProductEntity> orderProductEntityList = new ArrayList<>();
        request.getProducts().forEach(product -> {
            orderProductEntityList.add(
                    OrderProductEntity.builder()
                            .orderId(orderEntity.getId())
                            .productId(product.getProductId())
                            .count(product.getCount())
                            .build()
            );
        });

        orderProductService.save(orderProductEntityList);
    }

    protected List<ProductEntity> getProductEntities(List<OrderProductRequest> products){
        return productService.findAllByIds(
                products.stream()
                        .map(OrderProductRequest::getProductId)
                        .collect(Collectors.toList())
        );
    }

}
